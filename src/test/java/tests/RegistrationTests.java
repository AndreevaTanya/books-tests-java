package tests;

import java.util.Random;

import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import ru.st.selenium.pages.RegistrationModel;
import ru.st.selenium.pages.RegistrationPage;
import ru.st.selenium.pages.TestBase;

public class RegistrationTests extends TestBase {
	private String userName = "User" + (new Random()).nextInt(99999); 
		
	@Test
	public void CreateAccountTest() throws InterruptedException {
		
		 RegistrationPage page = new RegistrationPage(driver);
		 RegistrationModel newAccount = new RegistrationModel(userName, "First", "Last", "Male", "email@email.ru", "123", "123");
		 
		 page.RegistrationAndLogin(newAccount);
		 
		 AssertJUnit.assertEquals("Ошибка после регистрации", String.format("You are signed in as %s", userName), page.GetUserName());
	}
	
	@Test
	public void DifferentPasswordTest() throws InterruptedException {
		
		 RegistrationPage page = new RegistrationPage(driver);
		 RegistrationModel newAccount = new RegistrationModel(userName, "First", "Last", "Male", "email@email.ru", "123", "456");
		 
		 page.Registration(newAccount);
		 
		 AssertJUnit.assertTrue("Нет ошибки что пароль введен неверно", page.PasswordErrorIsVisible());
	}
	
	@Test
	public void NotTheCorrectFormatEmailTest() throws InterruptedException {
		
		 RegistrationPage page = new RegistrationPage(driver);
		 RegistrationModel newAccount = new RegistrationModel(userName, "First", "Last", "Male", "email", "123", "123");
		 
		 page.Registration(newAccount);
		 
		 AssertJUnit.assertTrue("Нет ошибки что email введен неверно", page.emailErrorIsVisible());
	}
}
