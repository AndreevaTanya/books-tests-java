package ru.st.selenium.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.st.selenium.util.Browser;

public class BasePage {
	protected WebDriver driver;

	protected Browser browser;
	
	protected BasePage(WebDriver driver)
    {
        this.driver = driver;
    }
		
	public boolean IsElementPresent(WebElement element)
	 {
	    	 boolean isPresent;
	         driver.manage().timeouts().implicitlyWait(40, TimeUnit.MILLISECONDS);
	         try
	         {
	             Point location = element.getLocation();
	             isPresent = true;
	         }
	         catch (Exception e)
	         {
	             isPresent = false;
	         }
	         driver.manage().timeouts().implicitlyWait(40, TimeUnit.MILLISECONDS);
	         return isPresent;
	    
	}
	
	public void ClickByElement(WebElement element)
	{
		WaitForElementPresent(element);
		element.click();
	}
	
	public void Input(WebElement element, String text)
	{
		if(text == null) return;
		WaitForElementPresent(element);
		element.clear();
		element.sendKeys(text);
	}
	
	public void Select(WebElement element, String text)
	{
		if(text == null) return;
		
		WaitForElementPresent(element);
		Select selector = new Select(element);
		selector.selectByVisibleText(text);
	}
	
	public String GetSelectCurrent(WebElement element)
	{
		Select selector = new Select(element);
		return selector.getFirstSelectedOption().getText();
	}
	
	public int GetCount(By locator)
	{
		try{
		if(!IsElementPresent(driver.findElement(locator)))
		{
			return 0;
		}
		else
		{
			return driver.findElements(locator).size();
		}
		}
		 catch(Exception  e){
	    		return 0;
	    	}
	}
	
	public String GetAtributeValue(WebElement elemen)
	{
		WaitForElementPresent(elemen);
		return elemen.getAttribute("value");
	}
	
	public void WaitForElementPresent(WebElement element)
    {
		WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement myDynamicElement = (new WebDriverWait(driver, 10))
        		  .until(ExpectedConditions.visibilityOf(element));
    }
	
}
