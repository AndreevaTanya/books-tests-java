package ru.st.selenium.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BooksPage extends BasePage {

	public BooksPage(WebDriver driver) {
		super(driver);
		 PageFactory.initElements(driver, this);
	}
	
	 private String username = "Tanya";
	 private String password = "123";
	 private String nameTestBook = "Test";
	 	
	 @FindBy(xpath = "//a[@href='/books/addbook']")
	 private WebElement addBookButton;
	 
	 @FindBy(xpath = "//a[text()='Books']")
	 private WebElement booksButton;
	 
	 @FindBy(id = "title")
	 private WebElement nameInput;
	 
	 @FindBy(id = "author")
	 private WebElement authorInput;
	 
	 @FindBy(id = "genre")
	 private WebElement genreInput;
	 
	 @FindBy(id = "year")
	 private WebElement yearInput;
	 
	 @FindBy(id = "language")
	 private WebElement languageSelect;
	 
	 @FindBy(xpath = "//button[text()='Add book']")
	 private WebElement saveBookButton;
	 
	 @FindBy(xpath = "(//*[@class='table table-striped']/tbody/tr/td/a)[1]")
	 private WebElement firstItem;
	 
	 @FindBy(name = "title")
	 private WebElement searchInput;
	 
	 @FindBy(xpath = "//button[text()='Search']")
	 private WebElement searchButton;
	 
	 @FindBy(xpath = "(//a[contains(@href, '/books/delete')])[1]")
	 private WebElement deleteFirstBookLink;
	 
	 @FindBy(xpath = "//*[text()='Required field']")
	 private WebElement errorText;
	 
	 @FindBy(xpath = "//*[@class='table table-striped']")
	 private WebElement list;
	 	 
	 private By itemsCount()
	 { return  By.xpath("//*[@class='table table-striped']/tbody/tr"); }
	 
	 private WebElement bookLink(String name)
	 {
		return driver.findElement(By.xpath(String.format("//a[text()='%s']", name)));
	 }
	 
	 private WebElement authorText(String name)
	 {
		return driver.findElement(By.xpath(String.format("(//a[text()='%s']/../../td)[2]", name)));
	 }
	 
	 private WebElement genreText(String name)
	 {
		return driver.findElement(By.xpath(String.format("(//a[text()='%s']/../../td)[3]", name)));
	 }
	 
	 private WebElement yearText(String name)
	 {
		return driver.findElement(By.xpath(String.format("(//a[text()='%s']/../../td)[4]", name)));
	 }
	 
	 private WebElement languageText(String name)
	 {
		return driver.findElement(By.xpath(String.format("(//a[text()='%s']/../../td)[5]", name)));
	 }
	 
	public void AddBook(NewBookModel book)
	{
		LoginPage page = new LoginPage(driver);
		page.Login(username, password);
		OpenAddBookPage();
		FillNewBookForm(book);	
		ClickByElement(saveBookButton);
	}
	
	public void OpenAddBookPage()
	{
		LoginPage page = new LoginPage(driver);
		page.Login(username, password);
		ClickByElement(addBookButton);
	}
	
	public void OpenBooksPage()
	{
		ClickByElement(booksButton);
	}
	
	public void OpenBook(String name)
	{
		ClickByElement(bookLink(name));
	}
	
	public String GetFirstBookInTheList()
	{
		return firstItem.getText();
	}
	
	public void DeleteBook(String name)
	{
		SearchBook(name);
		 DeleteFirstBook();
	}
	
	public void DeleteAllTestBook()
	{
		LoginPage page = new LoginPage(driver);
		page.Login(username, password);
		SearchBook(nameTestBook);
		DeleteAll();
	}
	
	public String GetAuthorByBook()
	{
		return GetAtributeValue(authorInput);		
	}
	
	public String GetAuthorByBookInList(String name)
	{
		return authorText(name).getText();		
	}
	
	public String GetGenreByBook()
	{
		return GetAtributeValue(genreInput);
	}
	
	public String GetGenreByBookInList(String name)
	{
		return genreText(name).getText();	
	}
	
	public String GetYearByBook()
	{
		return GetAtributeValue(yearInput);	
	}
	
	public String GetYearByBookInList(String name)
	{
		return yearText(name).getText();	
	}
	
	public String GetLanguageByBook()
	{
		return GetSelectCurrent(languageSelect);	
	}
	
	public String GetLanguageByBookInList(String name)
	{
		return languageText(name).getText();
	}
		
	public boolean IsErrorVisible()
	{
		return IsElementPresent(errorText);
	}
	
	public boolean IsBooksListPresent()
	{
		return IsElementPresent(list);
	}
	
	public void SearchBook(String Name)
	{
		Input(searchInput, Name);
		ClickByElement(searchButton);
	}
	
	public int GetCountBooks()
	{
		return GetCount(itemsCount());
	}
	
	private void FillNewBookForm(NewBookModel book)
	{
		Input(nameInput, book.Name);
		Input(authorInput, book.Author);
		Input(genreInput, book.Genre);
		Input(yearInput, book.Year);
		Select(languageSelect, book.Language);
	}
	
	 private void DeleteAll()
	 {
		 while(GetCountBooks() != 0)
		 {
			 DeleteFirstBook();
			 SearchBook(nameTestBook);
		 }
	 }
	 
	 private void DeleteFirstBook()
	 {
		 ClickByElement(deleteFirstBookLink);
			Alert alert = driver.switchTo().alert();
			alert.accept();
	 }
}
