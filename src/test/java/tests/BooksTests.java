package tests;

import java.util.Random;

import org.junit.After;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import ru.st.selenium.pages.BooksPage;
import ru.st.selenium.pages.NewBookModel;
import ru.st.selenium.pages.TestBase;

public class BooksTests extends TestBase {
	
	private String nameBook = "TestNameBook" + (new Random()).nextInt(99999); 
			
	@Test
	public void NewBookFirstInTheListTest() throws InterruptedException {
		
		BooksPage page = new BooksPage(driver);
		NewBookModel book = new NewBookModel(nameBook, "TestAuthor", "Novel", "2001", "English");

		page.AddBook(book);
		 
		AssertJUnit.assertEquals("После создания книга не первая в списке", nameBook, page.GetFirstBookInTheList());
	}
	
	@Test
	public void AddBookCheckInListTest() throws InterruptedException {
		
		BooksPage page = new BooksPage(driver);
		NewBookModel book = new NewBookModel(nameBook, "TestAuthor", "Novel", "2001", "English");
		
		page.AddBook(book);
		 
		AssertJUnit.assertEquals("Автор не соответсвует ожидаемому значению", book.Author, page.GetAuthorByBookInList(book.Name));
		AssertJUnit.assertEquals("Жанр не соответсвует ожидаемому значению", book.Genre, page.GetGenreByBookInList(book.Name));
		AssertJUnit.assertEquals("Год не соответсвует ожидаемому значению", book.Year, page.GetYearByBookInList(book.Name));
		AssertJUnit.assertEquals("Язык не соответсвует ожидаемому значению", book.Language, page.GetLanguageByBookInList(book.Name));
	}
	
	@Test
	public void AddBookCheckInsideTheBookTest() throws InterruptedException {
		
		BooksPage page = new BooksPage(driver);
		NewBookModel book = new NewBookModel(nameBook, "TestAuthor", "Novel", "2001", "English");
		
		page.AddBook(book);
		page.OpenBook(book.Name);
		 
		AssertJUnit.assertEquals("Автор не соответсвует ожидаемому значению", book.Author, page.GetAuthorByBook());
		AssertJUnit.assertEquals("Жанр не соответсвует ожидаемому значению", book.Genre, page.GetGenreByBook());
		AssertJUnit.assertEquals("Год не соответсвует ожидаемому значению", book.Year, page.GetYearByBook());
		AssertJUnit.assertEquals("Язык не соответсвует ожидаемому значению", book.Language, page.GetLanguageByBook());
	}
	
	@Test
	public void ValidationAddBookTest() throws InterruptedException {
		
		BooksPage page = new BooksPage(driver);
		NewBookModel book = new NewBookModel();
		
		page.AddBook(book);
		
		AssertJUnit.assertTrue("Нет ошибки что поля не заполнены", page.IsErrorVisible());
		AssertJUnit.assertEquals("Автор не соответсвует ожидаемому значению", "", page.GetAuthorByBook());
		AssertJUnit.assertEquals("Жанр не соответсвует ожидаемому значению", "", page.GetGenreByBook());
		AssertJUnit.assertEquals("Год не соответсвует ожидаемому значению", "", page.GetYearByBook());
		AssertJUnit.assertEquals("Язык не соответсвует ожидаемому значению", "Language", page.GetLanguageByBook());
	}
	
	@Test
	public void OpenBookPageTest() throws InterruptedException {
		
		BooksPage page = new BooksPage(driver);
		page.OpenAddBookPage();
		page.OpenBooksPage();
		
		AssertJUnit.assertTrue("Нет списка книг на странице", page.IsBooksListPresent());
	}
	
	@Test
	public void DeleteTestBooks() {
		
		BooksPage page = new BooksPage(driver);
		NewBookModel book = new NewBookModel(nameBook, "TestAuthor", "Novel", "2001", "English");
		page.AddBook(book);
		page.DeleteBook(book.Name);
		page.SearchBook(book.Name);
		AssertJUnit.assertEquals("После удаления остались записи", 0, page.GetCountBooks());
	}
	
	@AfterTest
	public void DeleteAllTestBooks() {
		
		BooksPage page = new BooksPage(driver);
		page.DeleteAllTestBook();
	}
}
