package ru.st.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {
	
	public LoginPage(WebDriver driver) {
		super(driver);
		 PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "username")
    private WebElement loginInput;
 
    @FindBy(name = "password")
    private WebElement passwordInput;
    
    @FindBy(xpath = "//*[text()='Sign in']")
    private WebElement signInButton;
	
	public void Login(String login, String password)
    {
		driver.get("http://localhost:9000/");
		if(IsElementPresent(loginInput))
		{
			loginInput.sendKeys(login);
			passwordInput.sendKeys(password);
			signInButton.submit();
		}
     }
}
