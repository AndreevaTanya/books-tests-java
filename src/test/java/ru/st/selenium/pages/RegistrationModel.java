package ru.st.selenium.pages;

public class RegistrationModel {
	
	public RegistrationModel(String name, String firstName, String lastName, String sex, String email, String password, String repeatPassword)
	{
		 this.Name = name;
		 this.firstName = firstName;
		 this.lastName = lastName;
		 this.Sex = sex;
		 this.Email = email; 
		 this.Password = password; 
		 this.RepeatPassword = repeatPassword; 
	}
	
	public String Name;
	  
	public String firstName;
	 
	public String lastName;
	  
	public String Sex;
	  
	public String Email;
	
	public String Password;
	
	public String RepeatPassword;
	
}
