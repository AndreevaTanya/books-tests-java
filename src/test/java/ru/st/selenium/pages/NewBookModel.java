package ru.st.selenium.pages;

public class NewBookModel {
	public NewBookModel(String name, String author, String genre, String year, String language)
	{
		 this.Name = name;
		 this.Author = author;
		 this.Genre = genre;
		 this.Year = year;
		 this.Language = language; 
	}
	
	public NewBookModel()
	{
		
	}

	  public String Name;
	  
	  public String Author;
	 
	  public String Genre;
	  
	  public String Year;
	  
	  public String Language;
}
