package ru.st.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage  extends BasePage {

	public RegistrationPage(WebDriver driver) {
		super(driver);
		 PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//a[text()='Create Account']")
    private WebElement createAccountLink;
	
	@FindBy(id = "userName")
    private WebElement userNameInput;
	
	@FindBy(id = "firstName")
    private WebElement firstNameInput;
	
	@FindBy(id = "lastName")
    private WebElement lastNameInput;
	
	@FindBy(xpath = "//input[@value='Female']")
    private WebElement femaleRadiobutton;
	
	@FindBy(xpath = "//input[@value='Male']")
    private WebElement maleRadiobutton;
	
	@FindBy(id = "email")
    private WebElement emailInput;
	
	@FindBy(id = "password")
    private WebElement passwordInput;
	
	@FindBy(id = "passwordConfirmation")
    private WebElement passwordConfirmationInput;
	
	@FindBy(xpath = "//button[text()='Create Account']")
    private WebElement saveAccountButton;
	
	@FindBy(xpath = "//div[@class='well container']/span")
    private WebElement codeText;
	
	@FindBy(id = "username")
    private WebElement codeinput;
	
	@FindBy(xpath = "//button[text()='Ok']")
    private WebElement okButton;
	
	@FindBy(xpath = "//*[@class='navbar-text navbar-right']")
    private WebElement userNameText;
	
	@FindBy(xpath = "//*[text()='Must be equal to password field']")
    private WebElement passwordErrorText;
	
	@FindBy(xpath = "//*[text()='Invalid email address']")
    private WebElement emailErrorText;
	
	public void Open()
	{
		driver.get("http://localhost:9000/registrations/registration");	
	}
	
	public String GetUserName()
	{
		return userNameText.getText();
	}
	
	public boolean PasswordErrorIsVisible()
	{
		return IsElementPresent(passwordErrorText);
	}
	
	public boolean emailErrorIsVisible()
	{
		return IsElementPresent(emailErrorText);
	}
	
	public void RegistrationAndLogin(RegistrationModel registration)
	{
		RegistrationAndInputCode(registration);
		LoginPage page = new LoginPage(driver);
		page.Login(registration.Name, registration.Password);
	}
		
	public void Registration(RegistrationModel registration)
	{
		Open();
		FillRegistrationForm(registration);
		ClickByElement(saveAccountButton);
	}
	
	private void RegistrationAndInputCode(RegistrationModel registration)
	{
		Registration(registration);
		InputCode();
		ClickByElement(okButton);
	}
		
	private void FillRegistrationForm(RegistrationModel registration)
	{
		Input(userNameInput, registration.Name);
		Input(firstNameInput, registration.firstName);
		Input(lastNameInput, registration.lastName);
		Input(emailInput, registration.Email);
		Input(passwordInput, registration.Password);
		Input(passwordConfirmationInput, registration.RepeatPassword);
		SeleckSex(registration.Sex);
	}
	
	private void InputCode()
	{
		Input(codeinput, codeText.getText());
	}
	
	private void SeleckSex(String sex)
	{
		if(sex == "Male")
		{
			ClickByElement(maleRadiobutton);
		}
		else
		{
			ClickByElement(femaleRadiobutton);
		}
	}
}
